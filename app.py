import random
import string
from datetime import datetime
from urllib.parse import urlparse

from flask import Flask, redirect, request, jsonify
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import NoResultFound

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:qwerty@localhost/flask-app'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'a really really really really long secret key'

db = SQLAlchemy(app)
ma = Marshmallow(app)


class UrlModel(db.Model):
    __tablename__ = 'Urls'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column('url', db.String(255), nullable=False, index=True)
    short_code = db.Column('short_code', db.String(8), nullable=False, index=True, unique=True)
    date = db.Column('date', db.DateTime, default=datetime.utcnow)

    def __init__(self, url, short_code):
        self.url = url
        self.short_code = short_code


class UrlSchema(ma.Schema):
    class Meta:
        fields = ('url', 'short_code')


url_schema = UrlSchema()
urls_schema = UrlSchema(many=True)


def generate_short_code():
    LETTERS = string.ascii_letters + string.digits
    return ''.join(random.sample(LETTERS, 8))


def presence_http(http_url):
    url = urlparse(http_url)
    return url.netloc + url.path


@app.route('/create', methods=['POST'])
def create_shorted_url():
    url = presence_http(request.json['url'])
    short_code = request.json['short_code']

    if short_code == '':
        short_code = generate_short_code()

    try:
        is_uniq = UrlModel.query.filter_by(short_code=short_code).one()
    except NoResultFound:
        created_url = UrlModel(url, short_code)
        db.session.add(created_url)
        db.session.commit()
        return url_schema.jsonify(created_url)
    else:
        return jsonify(message="URL is busy. Repeat")


@app.route('/<short_code>', methods=['GET'])
def redirect_to_primitive_url(short_code):
    url = UrlModel.query.filter_by(short_code=short_code).first()
    return url_schema.jsonify(url)
    # return redirect("http://" + url.url) - Instand Redirect


@app.route('/all', methods=['GET'])
def get_all():
    all_urls = UrlModel.query.all()
    result = urls_schema.dump(all_urls)
    return jsonify(result)


if __name__ == '__main__':
    app.run(port=8000, debug=True)
